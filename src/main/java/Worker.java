import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Worker {
    private String name;
    private String profession;
    private String email;
    private int phoneNumber ;
    private int age;
}
